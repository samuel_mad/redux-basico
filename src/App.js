import React from 'react';
import ContainerContador from './components/Contador/ContainerContador'
import ContainerBotonera from './components/Botonera/ContainerBotonera'
import './App.css';

function App() {
  return (
    <div className="App">
      <ContainerContador/>
      <ContainerBotonera/>
    </div>
  );
}

export default App;
