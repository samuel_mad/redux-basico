import {connect} from 'react-redux'
import Botonera from './Botonera'

const mapDispatchToProps = dispatch => {
  return {
    increment: () => dispatch({type:'INCREMENT'}),
    decrement: () => dispatch({type:'DECREMENT'}),
    reset: () => dispatch({type:'RESET'}) 
  }
}

export default connect(null, mapDispatchToProps)(Botonera)