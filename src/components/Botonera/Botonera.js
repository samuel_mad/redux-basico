import React from 'react'

const Botonera = (props) => (
  <React.Fragment>
    <button onClick={props.increment}>+</button>
    <button onClick={props.reset}>Reset</button>
    <button onClick={props.decrement}>-</button>
  </React.Fragment>
)
export default Botonera