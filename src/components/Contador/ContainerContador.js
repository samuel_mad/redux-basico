import {connect} from 'react-redux'
import Contador from './Contador'

const mapStateToProps = (state) => {
  return {contador: state.contador}
}
export default connect(mapStateToProps)(Contador)