const INITIAL_STATE = {
  contador: 0
}

export function contadorApp(state=INITIAL_STATE, action){
  switch (action.type) {
    case 'RESET':
      return {contador:0}
    case 'INCREMENT':
      return {contador: state.contador+1}
    case 'DECREMENT':
        return {contador: state.contador-1}
    default:
      return state
  }
}